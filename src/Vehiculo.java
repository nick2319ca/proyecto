import javax.swing.*;
import java.math.BigDecimal;

public class Vehiculo {

    public String Marca;
    public String Modelo;
    public BigDecimal Precio;


    public String getMarca(){
        return Marca;
    }
    public void setMarca(String Marca){
        this.Marca = Marca;
    }
    public String getModelo(){
        return Modelo;
    }
    public void setModelo(String Modelo){
        this.Modelo = Modelo;
    }
    public BigDecimal getPrecio(){
        return Precio;
    }
    public void setPrecio(BigDecimal Precio){
        this.Precio = Precio;
    }

    public static void main(String[] args) {

        JOptionPane.showMessageDialog(null,"Programa para añadir vehiculo a la lista");

        JOptionPane.showMessageDialog(null,"Ingresar los sigientes datos:\n 1.Marca\n 2.Modelo\n 3.Precio");

        String b = "no";

        Vehiculo Marca1 = new Vehiculo();
        Marca1.setMarca(JOptionPane.showInputDialog("Digite la Marca:"));
        System.out.println(Marca1.getMarca());

        Vehiculo Modelo1 = new Vehiculo();
        Modelo1.setModelo(JOptionPane.showInputDialog("Digita el Modelo:"));
        System.out.println(Modelo1.getModelo());

        Vehiculo Precio1 = new Vehiculo();
        Precio1.setPrecio(new BigDecimal(JOptionPane.showInputDialog("Digita el precio:")));
        System.out.println(Precio1.getPrecio());

        String a =JOptionPane.showInputDialog("es un camion?");

        if (!a.equals("no")){

            Camion Carga1 = new Camion();
            Carga1.setCapacidad_de_carga(Double.valueOf(JOptionPane.showInputDialog("Ingrese la carga del Camion en Kg")));
            String d = JOptionPane.showInputDialog("Quieres ver la lista?");
            if(!d.equals("no")){
                JOptionPane.showMessageDialog(null,"Marca:" + Marca1.getMarca() + "\nModelo:" + Modelo1.getModelo() + "\nPrecio:" + Precio1.getPrecio() +"\nCapacidad de carga:" + Carga1.getCapacidad_de_carga());

            }
        }else {

            String c = JOptionPane.showInputDialog("Es una furgoneta?");
            if (!c.equals("no")){
                Furgoneta Volumen1 = new Furgoneta();
                Volumen1.setVolumen_de_carga(Integer.valueOf(JOptionPane.showInputDialog("Ingresa el Volumen de carga:")));
                String d = JOptionPane.showInputDialog("Quiere ver la lista?");
                if (!d.equals("no")){
                    JOptionPane.showMessageDialog(null,"Marca:" + Marca1.getMarca() + "\nModelo:" + Modelo1.getModelo() + "\nPrecio:" + Precio1.getPrecio() +"\nVolumen de carga:" + Volumen1.getVolumen_de_carga());
                }

            }
            else {
                JOptionPane.showMessageDialog(null,"Entonces es un Auto");
                Auto Puerta1 = new Auto();
                Puerta1.setPuertas(Integer.valueOf(JOptionPane.showInputDialog("Ingrese el numero de puertas:")));
                System.out.println(Puerta1.getPuertas());
                String d = JOptionPane.showInputDialog("Quieres ver la lista?");
                if (!d.equals("no")){
                    JOptionPane.showMessageDialog(null,"Marca:" + Marca1.getMarca() + "\nModelo:" + Modelo1.getModelo() + "\nPrecio:" + Precio1.getPrecio() +"\nCantidad de puertas:" + Puerta1.getPuertas());
                }

            }
        }

//no tocar
    }
}